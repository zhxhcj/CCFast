﻿using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.WF;
using BP.Port;
using BP.Sys;

namespace BP.WF.Template
{
    /// <summary>
    /// 抄送属性
    /// </summary>
    public class FlowTabAttr : EntityMyPKAttr
    {
        #region 基本属性
        /// <summary>
        /// 标题
        /// </summary>
        public const string Name = "Name";
        /// <summary>
        /// 抄送内容
        /// </summary>
        public const string FK_Flow = "FK_Flow";
        /// <summary>
        /// 标记
        /// </summary>
        public const string Mark = "Mark";
        /// <summary>
        /// 是否启用
        /// </summary>
        public const string IsEnable = "IsEnable";
        public const string UrlExt = "UrlExt";
        public const string Icon = "Icon";
        public const string OrgNo = "OrgNo";
        public const string Tip = "Tip";
        public const string Idx = "Idx";
        #endregion
    }
    /// <summary>
    /// 抄送
    /// </summary>
    public class FlowTab : EntityMyPK
    {
        #region 属性
        public string Name
        {
            get
            {
                return this.GetValStrByKey(FlowTabAttr.Name);
            }
            set
            {
                this.SetValByKey(FlowTabAttr.Name, value);
            }
        }
        public string FK_Flow
        {
            get
            {
                return this.GetValStrByKey(FlowTabAttr.FK_Flow);
            }
            set
            {
                this.SetValByKey(FlowTabAttr.FK_Flow, value);
            }
        }
        public bool IsEnable
        {
            get
            {
                return this.GetValBooleanByKey(FlowTabAttr.IsEnable);
            }
            set
            {
                this.SetValByKey(FlowTabAttr.IsEnable, value);
            }
        }
        public string OrgNo
        {
            get
            {
                return this.GetValStrByKey(FlowTabAttr.OrgNo);
            }
            set
            {
                this.SetValByKey(FlowTabAttr.OrgNo, value);
            }
        }
        public string Tip
        {
            get
            {
                return this.GetValStrByKey(FlowTabAttr.Tip);
            }
            set
            {
                this.SetValByKey(FlowTabAttr.Tip, value);
            }
        }
        public string UrlExt
        {
            get
            {
                return this.GetValStrByKey(FlowTabAttr.UrlExt);
            }
            set
            {
                this.SetValByKey(FlowTabAttr.UrlExt, value);
            }
        }
        public string Mark
        {
            get
            {
                return this.GetValStrByKey(FlowTabAttr.Mark);
            }
            set
            {
                this.SetValByKey(FlowTabAttr.Mark, value);
            }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 抄送设置
        /// </summary>
        public FlowTab()
        {
        }
        /// <summary>
        /// 抄送设置
        /// </summary>
        /// <param name="mypk"></param>
        public FlowTab(string mypk)
        {
            this.MyPK = mypk;
            this.Retrieve();
        }
        /// <summary>
        /// 重写基类方法
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("WF_FlowTab", "流程功能");

                map.AddMyPK();
                map.AddTBString(FlowTabAttr.Name, null, "标签", true, true, 0, 100, 10, false);
                map.AddTBString(FlowTabAttr.FK_Flow, null, "流程编号", false, false, 0, 4, 10);
                map.AddTBString(FlowTabAttr.Mark, null, "标记", false, false, 0, 50, 10);
                map.AddTBString(FlowTabAttr.Tip, null, "Tip", false, false, 0, 200, 10);

                map.AddTBString(FlowTabAttr.UrlExt, null, "url链接", false, false, 0, 300, 10);
                map.AddTBString(FlowTabAttr.Icon, null, "Icon", false, false, 0, 50, 10);
                map.AddTBString(FlowTabAttr.OrgNo, null, "OrgNo", false, false, 0, 50, 10);
                map.AddTBInt(FlowTabAttr.Idx, 0, "Idx", true, true);


                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        protected override bool beforeInsert()
        {
            if (SystemConfig.CCBPMRunModel == CCBPMRunModel.Single)
            {
            }
            else
            {
                this.OrgNo = BP.Web.WebUser.OrgNo;
            }
            return base.beforeInsert();
        }
    }
    /// <summary>
    /// 抄送s
    /// </summary>
    public class FlowTabs : EntitiesMyPK
    {
        #region 获得方法.
        public string Default_Mover(string flowNo, string myks)
        {
            string[] ens = myks.Split(',');
            for (int i = 0; i < ens.Length; i++)
            {
                var enNo = ens[i];
                if (enNo.Contains("Default") == true)
                    continue;

                string sql = "UPDATE WF_FlowTab SET Idx=" + (i + 1) + " WHERE MyPK='" + enNo + "' AND FK_Flow='" + flowNo + "' ";
                DBAccess.RunSQL(sql);
            }
            return "移动成功..";
        }

        /// <summary>
        /// 给主页初始化数据.
        /// </summary>
        /// <param name="flowNo"></param>
        /// <returns></returns>
        public string Default_Init_bak(string flowNo)
        {

            string rptNo = "ND" + int.Parse(flowNo) + "Rpt";

            Flow fl = new Flow(flowNo);

            BP.WF.Data.GERpts rpts = new BP.WF.Data.GERpts();

        //    GEEntitys ens = new GEEntitys(rptNo);

            GenerWorkFlows ens = new GenerWorkFlows();
            BP.En.QueryObject qo = new QueryObject(ens);
            qo.AddWhere(GenerWorkFlowAttr.FK_Flow, flowNo);
            qo.addAnd();
            qo.AddWhere(GenerWorkFlowAttr.Emps, " LIKE ", "%" + BP.Web.WebUser.No + "%");
            qo.addAnd();
            qo.AddWhere(GenerWorkFlowAttr.OrgNo, BP.Web.WebUser.OrgNo);
            qo.addOrderBy("RDT");
            qo.Top = 100;
            qo.DoQuery();
            return ens.ToJson();
        }

        public string Default_Init(string flowNo)
        {
            string rptNo = "ND" + int.Parse(flowNo) + "Rpt";
            GEEntitys ens = new GEEntitys(rptNo);
            BP.En.QueryObject qo = new QueryObject(ens);
            qo.AddWhere(BP.WF.Data.GERptAttr.FlowEmps, " LIKE ", "%" + BP.Web.WebUser.No + "%");
            qo.addOr();
            qo.AddWhere(BP.WF.Data.GERptAttr.FlowStarter, BP.Web.WebUser.No );
            //    qo.AddWhere(GenerWorkFlowAttr.OrgNo, BP.Web.WebUser.OrgNo);
            qo.addOrderBy("RDT");
            qo.Top = 100;
            qo.DoQuery();
            return ens.ToJson();
        }
        public string Default_Init_MapAttrs(string flowNo)
        {
            string rptNo = "ND" + int.Parse(flowNo) + "Rpt";
            MapAttrs attrs = new MapAttrs(rptNo);
            return attrs.ToJson();

            //GEEntitys ens = new GEEntitys(rptNo);
            //BP.En.QueryObject qo = new QueryObject(ens);
            //qo.AddWhere(BP.WF.Data.GERptAttr.FlowEmps, " LIKE ", "%" + BP.Web.WebUser.No + "%");
            //qo.AddWhere(GenerWorkFlowAttr.OrgNo, BP.Web.WebUser.OrgNo);
            //qo.addOrderBy("RDT");
            //qo.Top = 100;
            //qo.DoQuery();
            //return ens.ToJson();
        }
        #endregion


        #region 方法
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new FlowTab();
            }
        }
        /// <summary>
        /// 抄送
        /// </summary>
        public FlowTabs() { }
        #endregion

        #region 为了适应自动翻译成java的需要,把实体转换成List.
        /// <summary>
        /// 转化成 java list,C#不能调用.
        /// </summary>
        /// <returns>List</returns>
        public System.Collections.Generic.IList<FlowTab> ToJavaList()
        {
            return (System.Collections.Generic.IList<FlowTab>)this;
        }
        /// <summary>
        /// 转化成list
        /// </summary>
        /// <returns>List</returns>
        public System.Collections.Generic.List<FlowTab> Tolist()
        {
            System.Collections.Generic.List<FlowTab> list = new System.Collections.Generic.List<FlowTab>();
            for (int i = 0; i < this.Count; i++)
            {
                list.Add((FlowTab)this[i]);
            }
            return list;
        }
        #endregion 为了适应自动翻译成java的需要,把实体转换成List.
    }
}
